http://askubuntu.com/questions/805662/how-to-start-and-stop-virtualbox-guests-via-command-line
##############################################################################################

vboxmanage startvm "vm name" --type gui|sdl|headless|separate
vboxmanage controlvm "vm name" pause|resume|reset|poweroff|savestate
vboxmanage list vms
vboxmanage list runningvms
