#!/bin/bash
#https://askubuntu.com/questions/160945/is-there-a-way-to-disable-a-laptops-internal-keyboard
Icon_on="/home/ali/Projects/os/scripts/keyboard_disable/enable.png"
Icon_off="/home/ali/Projects/os/scripts/keyboard_disable/disable.png"
fconfig=".keyboard" 

id=$(xinput --list | grep "AT Translated Set 2 keyboard" | cut -d'=' -f2 | cut -f1)

function is_disabled() {
    xinput --list --long | grep -A 1 "id=$id" | grep -q disabled
}

if is_disabled;
    then
        notify-send -i $Icon_on "Enabling keyboard..." \ "Keyboard: ON";
        echo "enable keyboard"
        xinput enable $id
    else
        notify-send -i $Icon_off "Disabling Keyboard" \ "Keyboard: OFF";
        echo "disable keyboard"
        xinput disable $id
fi