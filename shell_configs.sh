working_dir='/home/ali/Projects/os'

export PAGER='most'
export PATH=$PATH:$working_dir/scripts

# .local/bin for python packages installes with pip
export PATH=$PATH:~/.local/bin

alias push="git push"
alias pull="git pull"
alias tags="git for-each-ref --format='%(refname:short) ==> %(subject) %(body)' refs/tags"
alias freenet="sudo apt update && sudo apt -y upgrade"
alias ae="ssh ali@192.168.1.50"
alias er="ssh erfan@192.168.1.70"
alias dl="/home/ali/Downloads/aria2/down.sh"
